find_program(Python3_EXECUTABLE python3)
if (${Python3_EXECUTABLE-NOTFOUND})
    find_package(Python3 COMPONENTS Interpreter)
else()
    set(Python3_FOUND TRUE)
endif()

if (${Python3_FOUND})
    MESSAGE(STATUS "OK, Found Python3: ${Python3_EXECUTABLE}")
    set(CAN_RUN_TESTS "ON")
else()
    MESSAGE(WARNING "Fail, Did not find Python3, disabling tests.")
    set(CAN_RUN_TESTS "OFF")
endif()

set(BUILD_TESTS ${CAN_RUN_TESTS} CACHE BOOL "Build tests.")

if (${BUILD_TESTS} AND NOT ${CAN_RUN_TESTS})
    MESSAGE(FATAL_ERROR "Asked to build tests, but requirenments are not met.")
endif()

if (${BUILD_TESTS})
    add_executable(test_xor
      test_bitset.cpp
      test_propagator.cpp
      test_xor_detection_tree.cpp
      test_xor_detection.cpp)
    target_link_libraries(test_xor core_pbp gtest_main gmock)

    add_custom_target(run_test_xor
        DEPENDS test_xor
        WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
        COMMAND ./test_xor
      )

    add_executable(xorfinder_no_proof
      xorfinder.cpp)
    target_link_libraries(xorfinder_no_proof core_no_proof)

    add_executable(xorfinder_pbp
      xorfinder.cpp)
    target_link_libraries(xorfinder_pbp core_pbp)

    add_custom_target(test_xorfinder_pbp
        DEPENDS xorfinder_pbp
        WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
        COMMAND  ${CMAKE_COMMAND} -E env XORFINDER_BIN=./xorfinder_pbp ${Python3_EXECUTABLE} ${PROJECT_SOURCE_DIR}/test/integrationtest.py
      )

    add_executable(xorfinder_drat
      xorfinder.cpp)
    target_link_libraries(xorfinder_drat core_drat)

    add_custom_target(test_xorfinder_drat
        DEPENDS xorfinder_drat
        WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
        COMMAND  ${CMAKE_COMMAND} -E env XORFINDER_BIN=./xorfinder_drat ${Python3_EXECUTABLE} ${PROJECT_SOURCE_DIR}/test/integrationtest.py
      )

    add_executable(miniExample miniExample.cpp)
    target_link_libraries(miniExample core_pbp)

    add_executable(test_drat_generator
        prooflogging/drat/proof_generator.cpp)
    target_link_libraries(test_drat_generator core_pbp gtest_main)

    add_custom_target(test_drat_generate_tests
        DEPENDS test_drat_generator
        WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
        COMMAND ./test_drat_generator
      )

    add_custom_target(test_drat_check_proofs
        DEPENDS test_drat_generate_tests
        WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
        COMMAND ${Python3_EXECUTABLE} ${PROJECT_SOURCE_DIR}/test/prooflogging/drat/proof_testing.py
      )

    add_custom_target(test_drat
      DEPENDS test_drat_check_proofs
    )

    add_custom_target(test_all
      DEPENDS test_drat test_xorfinder_pbp run_test_xor test_xorfinder_drat
    )

    add_test(test_all
        COMMAND ${CMAKE_COMMAND} --build . --target test_all
        WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
      )

endif()