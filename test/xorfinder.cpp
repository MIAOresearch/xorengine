#include <iostream>
#include <vector>
#include <iostream>
#include <fstream>
#include <algorithm>

#include "test/Solver.h"
#include "xorengine/Xor.hpp"
#include "prooflogging/Proof.hpp"
#include "prooflogging/drat/drat_proof.hpp"
#include "prooflogging/pbp/pbp_proof.hpp"
#include "prooflogging/without/without_proof.hpp"

#include "XorEngine.hpp"


#include "xorengine/private/Logging.hpp"

using namespace xorp;
using namespace testImpl;

const std::streamsize ignore_no_limit = std::numeric_limits<std::streamsize>::max();

template<typename Types>
bool sortXor(Xor<Types>& a, Xor<Types>& b) {
    if (a.rhs != b.rhs) {
        return a.rhs < b.rhs;
    }

    if (a.lits.size() != b.lits.size()) {
        return a.lits.size() < b.lits.size();
    }

    for (size_t i = 0; i < a.lits.size(); ++i) {
        if (a.lits[i] != b.lits[i]) {
            return a.lits[i] < b.lits[i];
        }
    }

    return false;
}

namespace xorfinder {
class Solver: public testImpl::Solver {
public:
    proof::Proof<xorengine::UsedTypes>* proof = nullptr;

    virtual void conflict(const xorp::Reason<testImpl::Lit>& reason) {
        std::cout << "found conflict" << std::endl;
        #if defined(USE_PBP_PROOF)
            if (proof) {
                proof::pbp::ContradictionStep<xorengine::UsedTypes> step(*proof, reason.id);
            }
        #endif
    }
};
}

class Finder {
public:
    std::unique_ptr<proof::Proof<xorengine::UsedTypes>> proof = nullptr;
    std::string proofFileName = "proof.pbp";

    xorp::Solver<xorengine::UsedTypes>& solver;

    Finder(xorp::Solver<xorengine::UsedTypes>& _solver)
        : solver(_solver)
    {}

    std::vector<Clause> db;
    bool hasUnfinished = false;
    int32_t maxVar = 0;
    std::vector<Xor<xorengine::UsedTypes>> xors;

    void add(int32_t lit) {
        if (!hasUnfinished) {
            db.emplace_back();
            proof::ConstraintId id = proof->getNextFormulaConstraintId();
            db.back().id = id;
            hasUnfinished = true;
        }
        if (lit == 0) {
            hasUnfinished = false;
        } else {
            maxVar = std::max(maxVar, std::abs(lit));
            db.back().lits.emplace_back(lit);
        }
    }

    void parse(std::istream& in) {
        LOG(debug) << "start parssing" << EOM;
        std::ios_base::sync_with_stdio(false);
        in.tie(nullptr);

        uint64_t numVariables = 0;
        uint64_t numClauses = 0;
        size_t maxVar = 0;
        bool foundHeader = false;
        while (!in.eof()) {
            char next = in.peek();
            if (next == 'c') {
                in.ignore(ignore_no_limit,'\n');
            } else if (next == 'p') {
                foundHeader = true;
                const size_t headerLength = 5;
                char buffer[headerLength];
                in.read(buffer, headerLength);
                std::string s(buffer, headerLength);
                if (s != "p cnf") {
                    LOG(fatal) << "Expected 'p cnf' found " << s << EOM;
                }
                in >> numVariables >> numClauses;

                // auto* ptr = new proof::Proof<xorengine::UsedTypes>(proofFileName, solver, numClauses);
                // proof = std::unique_ptr<proof::Proof<xorengine::UsedTypes>>(ptr);
                proof = std::make_unique<proof::Proof<xorengine::UsedTypes>>(proofFileName, solver, numClauses);

                in.ignore(ignore_no_limit,'\n');
            } else {
                if (!foundHeader) {
                    LOG(fatal) << "Missing header." << EOM;
                }
                int32_t literal;
                in >> literal;
                if (!in.eof()) {
                    if (in.fail() || in.bad()) {
                        LOG(fatal) << "unexpected input" << EOM;
                    }
                    maxVar = std::max(maxVar, static_cast<size_t>(std::abs(literal)));
                    this->add(literal);
                    if (literal == 0) {
                        in.ignore(ignore_no_limit,'\n');
                    }
                }
            }
        }

        if (numVariables != maxVar) {
            LOG(warn) << "Number of variables in header does not match!" << EOM;
        }
        if (numClauses != db.size()) {
            LOG(warn) << "Number of clauses in header does not match!" << EOM;
        }

        LOG(debug) << "done parssing " << db.size() << " constraints" << EOM;
    }

    void findAll(){
        xorengine::Detector detector(maxVar, proof.get());

       for (Clause& clause: db) {
            if (clause.lits.size() > 1 && clause.lits.size() < 10) {
                detector.addClause(clause);
            }
        }

        xors = detector.findXors();
    }

    void checkDuplicates() {
        for (auto& x: xors) {
            std::sort(x.lits.begin(), x.lits.end());
            assert(x.lits.size() > 2);
        }
        std::sort(xors.begin(), xors.end(), sortXor<xorengine::UsedTypes>);
        auto it = std::adjacent_find(xors.begin(), xors.end());
        if (it != xors.end()) {
            LOG(warn) << "found dulicate!" << EOM;
        }
    }
};

int main(int argc, char const *argv[])
{
    xorfinder::Solver solver;
    xorp::Solver<xorengine::UsedTypes> wrapper(solver);

    Finder f(wrapper);
    if (argc == 3) {
        f.proofFileName = argv[2];
    }

    if (argc == 1) {
        f.parse(std::cin);
    } else {
        std::ifstream in(argv[1]);
        f.parse(in);
    }

    solver.maxVar = f.maxVar;
    solver.proof = f.proof.get();
    f.findAll();

    std::ofstream out("out.csv");

    out << "size" << std::endl;

    size_t xorSizes[11];
    for (size_t i = 0; i < 11; i++) {
        xorSizes[i] = 0;
    }

    size_t xorsMaxSize = 0;
    size_t nLarge = 0;
    for (auto& x: f.xors) {
        xorsMaxSize = std::max(x.lits.size(), xorsMaxSize);
        if (x.lits.size() > 10) {
            nLarge += 1;
        } else {
            xorSizes[x.lits.size()] += 1;
        }
    }

    for (size_t i = 0; i < 11; i++) {
        std::cout << "c statistic: xors_sized_" << i << ": " << xorSizes[i] << std::endl;
    }
    std::cout << "c statistic: xors_large: " << nLarge << std::endl;
    std::cout << "c statistic: num_xors: " << f.xors.size() << std::endl;
    std::cout << "c statistic: xors_max: " << xorsMaxSize << std::endl;

    if (argc == 3) {
        xorengine::Propagator propagator(wrapper, f.xors, f.proof.get());
        propagator.propagate();
    }

    if (f.xors.size() == 0) {
        return 1;
    } else {
        return 0;
    }
}
