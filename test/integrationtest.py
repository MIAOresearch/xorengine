import unittest
import os

from pathlib import Path

from veripb import run, InvalidProof, ParseError
from veripb.utils import Settings as MiscSettings
from veripb.verifier import Verifier

class TestIntegration(unittest.TestCase):
    binary = os.environ.get("XORFINDER_BIN", "./xorfinder")

    def run_single(self, formulaPath, requireUnsat = False):
        print(self.binary, formulaPath)
        result = os.system("%s %s"%(self.binary, formulaPath))
        print(os.WIFEXITED(result))
        if not os.WIFEXITED(result):
            self.fail("Xorfinder crashed (%i)."%(result))
        elif os.WEXITSTATUS(result) == 1:
            self.fail("xorfinder did not find any xors.")
        elif os.WEXITSTATUS(result) != 0:
            self.fail("Error occured in xorfinder.")


        proofPath = Path("proof.pbp")
        print("veripb %s %s"%(formulaPath, proofPath))

        drat = self.binary.endswith("drat")
        miscSettings = MiscSettings({"cnf": True, "drat": drat})
        verifierSettings = Verifier.Settings({"trace": True, "requireUnsat": requireUnsat})
        with formulaPath.open() as formula:
            with proofPath.open() as proof:
                run(formula, proof, verifierSettings = verifierSettings, miscSettings = miscSettings)

    def correct_proof(self, formulaPath):
        self.run_single(formulaPath, requireUnsat = False)

    def correct_proof_unsat(self, formulaPath):
        self.run_single(formulaPath, requireUnsat = True)

    def incorrect_proof(self, formulaPath):
        try:
            self.run_single(formulaPath)
        except InvalidProof as e:
            pass
        else:
            self.fail("Proof should be invalid.")

    def parsing_failure(self, formulaPath):
        try:
            self.run_single(formulaPath)
        except ParseError as e:
            pass
        else:
            self.fail("Parsing should fail.")

def create(formulaPath, helper):
    def fun(self):
        helper(self, formulaPath)
    return fun

def findProblems(globExpression):
    current = Path(__file__).parent
    files = current.glob(globExpression)
    files = [f for f in files if f.suffix in [".cnf",".opb"] and f.is_file()]
    return files

sat = findProblems("integrationtests/sat/*.*")
for file in sat:
    method = create(file, TestIntegration.correct_proof)
    method.__name__ = "test_%s"%(file.stem)
    setattr(TestIntegration, method.__name__, method)

unsat = findProblems("integrationtests/unsat/*.*")
for file in unsat:
    method = create(file, TestIntegration.correct_proof_unsat)
    method.__name__ = "test_%s"%(file.stem)
    setattr(TestIntegration, method.__name__, method)


if __name__=="__main__":
    try:
        import pytest
    except NameError:
        exit(unittest.main())
    else:
        exit(pytest.main([__file__]))