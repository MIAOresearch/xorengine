#pragma once

#include "xorengine/private/assert.hpp"

class BitSet {
private:
    typedef uint32_t StoreType;
    static const size_t storeTypeSize = sizeof(StoreType) * 8;
    // note that 1 might not have the same type as StoreType, this can
    // cause issues if we write things like "1 << (storeTypeSize - 1)"
    // as the type of 1 might not support shifting as many bits as
    // StoreType has
    static const StoreType one = 1;

    std::vector<StoreType> data;

public:
    size_t _size = 0;

    class Iterator {
    private:
        BitSet& bitSet;
        size_t i;
        size_t bit;
        StoreType mask;

    public:
        Iterator(BitSet& _bitSet, size_t pos)
            : bitSet(_bitSet)
        {
            setCol(pos);
            init();
        }

        Iterator(BitSet& _bitSet, size_t _i, size_t _bit)
            : bitSet(_bitSet)
            , i(_i)
            , bit(_bit)
        {
            init();
        }

        size_t operator*() {
            return col();
            // size_t k = storeTypeSize - 1 - bit;
            // return bitSet.data[i] & (one << k);
        }

        void init() {
            mask = BitSet::one << (BitSet::storeTypeSize - 1);
            mask >>= bit;
            if (!(bitSet.data[i] & mask)) {
                ++(*this);
            }
        }

        bool nextBlock() {
            bit = 0;
            mask = BitSet::one << (BitSet::storeTypeSize - 1);
            i += 1;
            for (; i < bitSet.data.size(); ++i) {
                if (bitSet.data[i]) {
                    return true;
                }
            }
            return false;
        }

        void operator++() {
            while (true) {
                bit += 1;
                mask >>= 1;
                if (bit == storeTypeSize && !nextBlock()) {
                    setCol(bitSet.size());
                    return;
                } else if (bitSet.data[i] & mask) {
                    return;
                }
            }
        }

        bool operator==(const Iterator& other) const {
            return (&bitSet == &other.bitSet) && (i == other.i) && (bit == other.bit);
        }

        bool operator!=(const Iterator& other) const {
            return !(*this == other);
        }

        void setCol(size_t pos) {
            i   = pos / storeTypeSize;
            bit = pos % storeTypeSize;
        }

        size_t col() {
            return i * storeTypeSize + bit;
        }
    };

    BitSet(size_t _size)
    {
        resize(_size);
    }

    BitSet(const BitSet& other)
        : data(other.data)
        , _size(other.size())
    {}

    void resize(size_t newSize) {
        _size = newSize;
        data.resize(newSize / storeTypeSize + 1, 0);
        data.shrink_to_fit();
    }

    size_t size() const {
        return _size;
    }

    void set(size_t i) {
        Iterator it(*this, i);
        size_t j = i / storeTypeSize;
        size_t k = storeTypeSize - 1 - i % storeTypeSize;

        data[j] |= one << k;
    }



    void setTo(size_t i, bool value) {
        if (value) {
            set(i);
        } else {
            unset(i);
        }
    }

    void resetToTrue() {
        StoreType value = 0;

        value = ~value;
        for (StoreType& x:data) {
            x = value;
        }
    }

    void reset() {
        StoreType value = 0;
        for (StoreType& x:data) {
            x = value;
        }
    }

    void unset(size_t i) {
        size_t j = i / storeTypeSize;
        size_t k = storeTypeSize - 1 - i % storeTypeSize;
        data[j] &= ~(one << k);
    }

    // void flip(size_t i) {
    //     size_t j = i / storeTypeSize;
    //     size_t k = storeTypeSize - 1 - i % storeTypeSize;
    //     data[j] ^= one << k;
    // }

    bool operator[](size_t i) {
        size_t j = i / storeTypeSize;
        size_t k = storeTypeSize - 1 - i % storeTypeSize;
        return (data[j]) & (one << k);
    }

    BitSet& operator &=(const BitSet& other){
        assert(this->data.size() == other.data.size());

        auto otherIt = other.data.begin();
        for (StoreType& mine:data) {
            mine &= *otherIt;
            ++otherIt;
        }
        return *this;
    }

    BitSet& operator ^=(const BitSet& other){
        assert(this->data.size() == other.data.size());

        auto otherIt = other.data.begin();
        for (StoreType& mine:data) {
            mine ^= *otherIt;
            ++otherIt;
        }
        return *this;
    }

    void zeroOutLeftOver() {
        size_t leftover = storeTypeSize - size() % storeTypeSize;
        StoreType mask = 0;
        mask = ~mask;
        data.back() &= mask << leftover;
    }

    bool popcountMod2() {
        StoreType sum = 0;
        zeroOutLeftOver();

        for (StoreType& x:data) {
            sum ^= x;
        }

        size_t remainingSize = storeTypeSize;
        while (remainingSize != 1) {
            remainingSize = remainingSize / 2;
            sum ^= (sum >> remainingSize);
        }
        return sum & one;
    }

    Iterator findAny() {
        zeroOutLeftOver();

        size_t i = 0;
        for (; i < data.size(); i++) {
            if (data[i] != 0) {
                break;
            }
        }

        if (i < data.size()) {
            StoreType x = data[i];
            size_t msb = 0;
            while (x >>= one) {++msb;}
            size_t bit = storeTypeSize - msb - 1;
            assert((*this)[i * storeTypeSize + bit]);
            return BitSet::Iterator(*this, i, bit);
        } else {
            return BitSet::Iterator(*this, this->size());
        }

    };

    BitSet::Iterator begin() {
        return BitSet::Iterator(*this, 0);
    };

    BitSet::Iterator end() {
        return BitSet::Iterator(*this, this->size());
    };

    friend
    std::ostream& operator<<(std::ostream& os, const BitSet& v);
};