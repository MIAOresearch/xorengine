#pragma once

#include "prooflogging/ConstraintId.hpp"
#include "prooflogging/without/without_proof.hpp"

namespace proof {
namespace without {
namespace drat_rules {
    template<typename Types, typename T>
    ConstraintId add(Proof<Types>& proof, T begin, T end) {
        return ConstraintId{1};
    }

    template<typename Types, typename T>
    void del(Proof<Types>&, ConstraintId, T, T) {

    }

    template<typename Types>
    ConstraintId contradiction(Proof<Types>& proof) {
        typename Types::Lit* l = nullptr;
        return add(proof, l, l);
    }
}}}