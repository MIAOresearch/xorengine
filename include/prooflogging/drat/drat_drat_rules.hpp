#pragma once

#include "prooflogging/ConstraintId.hpp"
#include "prooflogging/drat/drat_proof.hpp"

namespace proof {
namespace drat {
namespace drat_rules {
    template<typename Types, typename T>
    ConstraintId add(Proof<Types>& proof, T begin, T end) {
        proof.add(begin, end);
        return ConstraintId{1};
    }

    template<typename Types, typename T>
    void del(Proof<Types>& proof, ConstraintId, T begin, T end) {
        proof.del(begin, end);
    }

    template<typename Types>
    ConstraintId contradiction(Proof<Types>& proof) {
        typename Types::Lit* l = nullptr;
        return add(proof, l, l);
    }
}}}