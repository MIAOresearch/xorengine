#pragma once

namespace proof {
    namespace pbp {
        template<typename Types>
        class Proof;
    }

    namespace drat {
        template<typename Types>
        class Proof;
    }

    namespace without {
        template<typename Types>
        class Proof;
    }
}

#if defined(USE_DRAT_PROOF) && defined(USE_PBP_PROOF)
    #error Cant use USE_DRAT and USE_PBP at the same time
#endif

#if defined(USE_PBP_PROOF)
    namespace proof {
        template<typename Types>
        using Proof = proof::pbp::Proof<Types>;
    }
    #define USE_ANY_PROOF
#endif

#if defined(USE_DRAT_PROOF)
    namespace proof {
        template<typename Types>
        using Proof = proof::drat::Proof<Types>;
    }
    #define USE_ANY_PROOF
#endif

#if !defined(USE_ANY_PROOF)
    namespace proof {
        template<typename Types>
        using Proof = proof::without::Proof<Types>;
    }
#endif